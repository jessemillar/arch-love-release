A minimal Docker image for [love-release](https://github.com/MisterDA/love-release) automation. Includes [the itch.io butler](https://itch.io/docs/butler/) and [ghr](https://github.com/tcnksm/ghr) for publishing releases to GitHub and [itch.io](https://itch.io). [oniietzschan/arch-love-release](https://github.com/oniietzschan/arch-love-release) was used as a starting point.

Docker image is available on DockerHub as [jessemillar/arch-love-release](https://hub.docker.com/r/jessemillar/arch-love-release/).

See [this file for example usage](https://github.com/jessemillar/the-staff-of-lewis/blob/master/.circleci/config.yml).
